(function() {
    //contenu de la page js encapsulé
    var app = angular.module('app', ['ngRoute'])
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/accueil', { template: '<h2>Bienvenue dans la boutique de Matthieu </h2><p>Ici vous trouverez la garde robe de Matthieu</p><hr/>' })
                .when('/produits', { templateUrl: 'vues/listeProduits.html' })
                .when('/produit/:idProduit', {
                    templateUrl: 'vues/produit.html',
                    controller: 'produitCtrls',
                    controllerAs: 'lpctrl'
                })
                .when('/panier', { templateUrl: 'vues/panier.html' })
                .otherwise({ redirectTo: '/accueil' });

        }]);
})();