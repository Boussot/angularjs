(function() {
    'use strict';

    angular
        .module('app')
        .controller('produitCtrls', ControllerController);

    ControllerController.$inject = ['$scope', '$routeParams', 'produitSrvc'];

    function ControllerController($scope, $routeParams, produitSrvc) {
        var vm = this;
        var _id = $routeParams.idProduit;
        this.p = produitSrvc.listeProduit;
        produitSrvc.loadProduitAsync().then(function(r) {
            vm.produit = produitSrvc.getProduit(_id)
        })


    }
})();