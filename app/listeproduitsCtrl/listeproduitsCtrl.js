angular.module('app').controller('listeproduitsCtrl', ['$scope', 'produitSrvc',
    function($scope, produitSrvc) {
        var vm = this;

        this.listeProduit = produitSrvc.listeProduit;
        produitSrvc.loadProduitAsync();

    }
]);