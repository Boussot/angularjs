(function() {
    'use strict';

    angular
        .module('app')
        .service('produitSrvc', ['$http', '$q', function($http, $q) {
            var vm = this;
            vm.listeProduit = [];
            var _isloaded = false;
            var _endload = false;
            var _loadProduit = function() {
                _isloaded = true;
                return $http.get('http://localhost:775/produit').then(function(response) {
                        _endload = true;
                        response.data.map(function(unElement) {
                            vm.listeProduit.push(unElement);

                        });
                    },
                    function(response) {
                        console.log(response);
                    });
            };

            vm.getProduit = function(id) {

                for (var i = 0; vm.listeProduit.length > i; i++) {
                    if (vm.listeProduit[i].id == id)
                        return vm.listeProduit[i];
                }
                return null;

            };
            this.loadProduitAsync = function() {
                if (!_isloaded) return _loadProduit();
                var e = _endload;
                return $q(function(resolve, reject) {
                    setTimeout(function(params) { while (!e) setTimeout('', 10); }, 1000);
                    if (!e) reject();
                    else resolve();

                });
            };
            //vm.loadProduit();


        }]);


})();