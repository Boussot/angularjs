(function() {
    'use strict';

    angular
        .module('app')
        .filter('euroCurrency', function() {
            function euroCurrencyFilter(value, currencyChar, precision) {
                var ret = '';
                var round = Math.round(value * Math.pow(10, precision)) /
                    Math.pow(10, precision);
                ret += round + ' ' + currencyChar;
                return ret;
            }

            return euroCurrencyFilter;
        })
        .filter('reduc', function() {
            function reducFilter(value, reduction) {
                var ret = 0.0;
                var reduction = (reduction !== undefined ? reduction : 20);

                return value - value * (reduction / 100);
            }

            return reducFilter;
        });

})();