(function() {
    'use strict';

    angular
        .module('app')
        .controller('panierCtrl', ControllerController);

    ControllerController.$inject = ['$scope', '$routeParams', 'produitSrvc', 'quantite'];

    function ControllerController($scope, $routeParams, produitSrvc, quantite) {
        var vm = this;
        var _id = $routeParams.idProduit;
        this.p = produitSrvc.listeProduit;
        produitSrvc.loadProduitAsync().then(function(r) {
            vm.produit = produitSrvc.getProduit(_id);
        });
    };
})();